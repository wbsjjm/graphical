document.write(`
    <div class="mdui-appbar mdui-color-grey-800">
        <div class="mdui-tab mdui-color-theme-dark mdui-color-grey-800" mdui-tab>
            <a class="mdui-ripple mdui-ripple-white">Box3图形化编辑器 测试版</a>
            <a href="#tb1" class="mdui-ripple mdui-ripple-white">项目</a>
            <a onclick="showjs()" class="mdui-typo-title mdui-color-grey-800 mdui-btn-raised">生成js</a>
            <div class="mdui-toolbar-spacer"></div>
            <a href="#tb2" class="mdui-ripple mdui-ripple-white">by 我不是吉吉喵</a>
        </div>
        <div id="tb1">
            <button onclick="ptjs('s')" class="mdui-typo-title mdui-color-grey-800 mdui-btn-raised">保存</button>
            <button onclick="ptjs('d')" class="mdui-typo-title mdui-color-grey-800 mdui-btn-raised">导入</button>
        </div>
        <div id="tb2">
            <p>版权归<b>我不是吉吉喵</b>所有</p>
        </div>
    </div>
    <p2>&nbsp;</p2>
    <div id="blocklyDiv" style="height:900px;width:1900px;">
    <xml id="toolbox" style="display: none">
        <category name="逻辑" colour="%{BKY_LOGIC_HUE}">
            <block type="controls_if">
            </block>
            <block type="controls_if">
                <mutation else="1"></mutation>
            </block>
            <block type="controls_if">
                <mutation elseif="1" else="1"></mutation>
            </block>
            <block type="logic_wait"></block>
            <block type="logic_compare">
            </block>
            <block type="logic_operation">
            </block>
            <block type="logic_negate">
            </block>
            <block type="logic_boolean">
            </block>
            <block type="logic_null">
            </block>
            <block type="logic_ternary">
            </block>
            <block type="logic_intext"></block>
        </category>
        <category name="循环" colour="%{BKY_LOOPS_HUE}">
            <block type="controls_repeat_ext">
                <value name="TIMES">
                    <shadow type="math_number">
                        <field name="NUM">
                            10
                        </field>
                    </shadow>
                </value>
            </block>
            <block type="controls_whileUntil">
            </block>
            <block type="controls_for">
                <value name="FROM">
                    <shadow type="math_number">
                        <field name="NUM">
                            1
                        </field>
                    </shadow>
                </value>
                <value name="TO">
                    <shadow type="math_number">
                        <field name="NUM">
                            10
                        </field>
                    </shadow>
                </value>
                <value name="BY">
                    <shadow type="math_number">
                        <field name="NUM">
                            1
                        </field>
                    </shadow>
                </value>
            </block>
            <block type="controls_forEach">
            </block>
            <block type="controls_flow_statements">
            </block>
        </category>
        <category name="数学" colour="%{BKY_MATH_HUE}">
            <block type="math_number">
                <field name="NUM">
                    123
                </field>
            </block>
            <block type="math_arithmetic">
                <value name="A">
                    <shadow type="math_number">
                        <field name="NUM">
                            1
                        </field>
                    </shadow>
                </value>
                <value name="B">
                    <shadow type="math_number">
                        <field name="NUM">
                            1
                        </field>
                    </shadow>
                </value>     
            </block>
            <block type="math_single">
                <value name="NUM">
                    <shadow type="math_number">
                        <field name="NUM">
                            9
                        </field>
                    </shadow>
                </value>
            </block>
            <block type="math_trig">
                <value name="NUM">
                    <shadow type="math_number">
                        <field name="NUM">
                            45
                        </field>
                    </shadow>
                </value>
            </block>
            <block type="math_constant">
            </block>
            <block type="math_number_property">
                <value name="NUMBER_TO_CHECK">
                    <shadow type="math_number">
                        <field name="NUM">
                            0
                        </field>
                    </shadow>
                </value>
            </block>
            <block type="math_round">
                <value name="NUM">
                    <shadow type="math_number">
                        <field name="NUM">
                            3.1
                        </field>
                    </shadow>
                </value>
            </block>
            <block type="math_on_list">
            </block>
            <block type="math_modulo">
                <value name="DIVIDEND">
                    <shadow type="math_number">
                        <field name="NUM">
                            64
                        </field>
                    </shadow>
                </value>
                <value name="DIVISOR">
                    <shadow type="math_number">
                        <field name="NUM">
                            10
                        </field>
                    </shadow>
                </value>
            </block>
            <block type="math_constrain">
                <value name="VALUE">
                    <shadow type="math_number">
                        <field name="NUM">
                            50
                        </field>
                    </shadow>
                </value>
                <value name="LOW">
                    <shadow type="math_number">
                        <field name="NUM">
                            1
                        </field>
                    </shadow>
                </value>
                <value name="HIGH">
                    <shadow type="math_number">
                        <field name="NUM">
                            100
                        </field>
                    </shadow>
                </value>
            </block>
            <block type="math_random_int">
                <value name="FROM">
                    <shadow type="math_number">
                        <field name="NUM">
                            1
                        </field>
                    </shadow>
                </value>
                <value name="TO">
                    <shadow type="math_number">
                        <field name="NUM">
                            100
                        </field>
                    </shadow>
                </value>
            </block>
        </category>
        <category name="文本" colour="%{BKY_TEXTS_HUE}">
            <block type="text"></block>
            <block type="text_join"></block>
            <block type="text_append">
                <value name="TEXT">
                    <shadow type="text"></shadow>
                </value>
            </block>
            <block type="text_length">
                <value name="VALUE">
                    <shadow type="text">
                        <field name="TEXT">abc</field>
                    </shadow>
                </value>
            </block>
            <block type="text_isEmpty">
                <value name="VALUE">
                    <shadow type="text">
                        <field name="TEXT"></field>
                    </shadow>
                </value>
            </block>
            <block type="text_indexOf">
                <value name="VALUE">
                    <block type="variables_get">
                        <field name="VAR">文本</field>
                    </block>
                </value>
                <value name="FIND">
                    <shadow type="text">
                        <field name="TEXT">abc</field>
                    </shadow>
                </value>
            </block>
            <block type="text_charAt">
                <value name="VALUE">
                    <block type="variables_get">
                        <field name="VAR">文本</field>
                    </block>
                </value>
            </block>
            <block type="text_getSubstring">
                <value name="STRING">
                    <block type="variables_get">
                        <field name="VAR">文本</field>
                    </block>
                </value>
            </block>
            <block type="text_changeCase">
                <value name="TEXT">
                    <shadow type="text">
                        <field name="TEXT">abc</field>
                    </shadow>
                </value>
            </block>
            <block type="text_trim">
                <value name="TEXT">
                    <shadow type="text">
                        <field name="TEXT">abc</field>
                    </shadow>
                </value>
            </block>
        </category>
        <category name="列表" colour="%{BKY_LISTS_HUE}">
            <block type="lists_create_with">
                <mutation items="0"></mutation>
            </block>
            <block type="lists_create_with"></block>
            <block type="lists_repeat">
                <value name="NUM">
                    <shadow type="math_number">
                        <field name="NUM">5</field>
                    </shadow>
                </value>
            </block>
            <block type="lists_length"></block>
            <block type="lists_isEmpty"></block>
            <block type="lists_indexOf">
                <value name="VALUE">
                    <block type="variables_get">
                        <field name="VAR">列表</field>
                    </block>
                </value>
            </block>
            <block type="lists_getIndex">
                <value name="VALUE">
                    <block type="variables_get">
                        <field name="VAR">列表</field>
                    </block>
                </value>
            </block>
            <block type="lists_setIndex">
                <value name="LIST">
                    <block type="variables_get">
                        <field name="VAR">列表</field>
                    </block>
                </value>
            </block>
            <block type="lists_getSublist">
                <value name="LIST">
                    <block type="variables_get">
                        <field name="VAR">列表</field>
                    </block>
                </value>
            </block>
            <block type="lists_split">
                <value name="DELIM">
                    <shadow type="text">
                        <field name="TEXT">,</field>
                    </shadow>
                </value>
            </block>
            <block type="lists_sort"></block>
        </category>
        <sep></sep>
        <category name="Box3世界" colour="270">
            <block type="position"></block>
            <block type="world_setarg"></block>
            <block type="world_say"></block>
        </category>
        <category name="Box3实体" colour="290">
            <block type="entity_setposition"></block>
            <block type="entity_getbytag"></block>
            <block type="entity_hurt"></block>
            <block type="entity_setarg"></block>
            <block type="entity_getarg"></block>
            <block type="entity_say"></block>
            <block type="entity_getposition"></block>
            <block type="entity_foreach"></block>
            <block type="entity_isplayer"></block>
            <block type="entity_setvelocity"></block>
            <block type="entity_enabledamage"></block>
            <block type="entity_showhealthbar"></block>
            <block type="entity_enableinteract"></block>
        </category>
        <category name="Box3事件" colour="280">
            <block type="event_onplayerjoin"></block>
            <block type="event_ontick"></block>
            <block type="event_onentitycontact"></block>
            <block type="event_onentityclick"></block>
            <block type="event_onchat"></block>
            <block type="event_ondie"></block>
            <block type="event_init"></block>
            <block type="event_var_message"></block>
            <block type="event_var_clicker"></block>
            <block type="event_var_other"></block>
            <block type="event_var_entity"></block>
        </category>
        <category name="Box3颜色" colour="188">
            <block type="rgbcolor"></block>
            <block type="rgbacolor"></block>
        </category>
        <category name="Box3玩家" colour="90">
            <block type="entity_getplayer"></block>
            <block type="player_getname"></block>
            <block type="player_setfly"></block>
            <block type="player_setvisible"></block>
            <block type="player_setcolor"></block>
            <block type="player_direct"></block>
            <block type="player_setarg"></block>
            <block type="player_setspawnpoint"></block>
            <block type="player_setattr"></block>
            <block type="player_movestate"></block>
            <block type="player_walkstate"></block>
            <block type="player_forcerespawn"></block>
            <block type="player_getattr"></block>
        </category>
        <category name="Box3方块" colour="300">
            <block type="block_getid1"></block>
            <block type="block_getid2"></block>
            <block type="block_set"></block>
            <block type="block_get"></block>
            <block type="block_getid3"></block>
            <block type="block_getid4"></block>
            <block type="block_getid5"></block>
            <block type="block_getid6"></block>
        </category>
        <sep></sep>
        <category name="变量" colour="%{BKY_VARIABLES_HUE}" custom="VARIABLE">
        </category>
        <category name="函数" colour="%{BKY_PROCEDURES_HUE}" custom="PROCEDURE">
        </category>
        <category name="插件" colour="21">
            <block type="adv_runcode"></block>
            <block type="adv_blockclamp"></block>
            <block type="debug_log"></block>
            <block type="debug_warn"></block>
            <block type="debug_error"></block>
            <block type="debug_clear"></block>
        </category>
        <category name="帮助" colour="77">
            <label>使用须知</label>
            <label>1.请勿在任何积木上添加注释,否则将导致生成的代码功能不齐全或报错!!!</label>
            <label>2.生成的JS为Node.js型,若要加密请勾选node.js,否则会报错!!!</label>
            <block type="wbsjjm3"></block>
            <block type="wbsjjm"></block>
            <block type="wbsjjm2"></block>
        </category>
    </xml>
    </div>
    <xml id="sd" style="display:none" xmlns="https://developers.google.com/blockly/xml"><block type="event_init" id="[\`?e_V[Kq#Y=g{hC{fsg" x="306" y="165"><next><block type="event_onplayerjoin" id="ybS!eV/4RbA#Xmvr%r~K"><statement name="DO"><block type="controls_if" id="zOF2!E44[.;k~lI^{!U;"><mutation else="1"></mutation><value name="IF0"><block type="logic_compare" id="P07l*g#8$HEzFkccm;ax"><field name="OP">EQ</field><value name="A"><block type="text" id="/\`%Hsl%@G7Qn2UpXVdsx"><field name="TEXT">我不是吉吉喵</field></block></value><value name="B"><block type="text_join" id="S(b+*Tm|,*by6DMY4bPJ"><mutation items="1"></mutation><value name="ADD0"><block type="player_getname" id="w:dehvS@~8bpI{rr%=-W"><value name="P"><block type="entity_getplayer" id="{N8NQb1_jbX9CU+q9LDa"><value name="E"><block type="event_var_entity" id="_ZtD.P0%?^mqMYCbp{KT"></block></value></block></value></block></value></block></value></block></value><statement name="DO0"><block type="world_say" id="5:\`Wl8Xl$nWg@g^[;/)m"><value name="M"><block type="text" id="7BH3ph0$vkZ*\`kuoX.^c"><field name="TEXT">作者来了，欢迎！</field></block></value></block></statement><statement name="ELSE"><block type="world_say" id="GtcAops)ptAp5m+_mTFu"><value name="M"><block type="text" id="#EC]+hY%}:v*u}7XQ(+T"><field name="TEXT">欢迎来到本游戏。</field></block></value></block></statement></block></statement></block></next></block></xml>
    <script src="core.js"></script>
    <script src="block.js"></script>
    <script src="javascript_compressed.js"></script>
    <script src="zh-hans.js"></script>
    <script src="wbsjjm_block.js"></script>
    <script src="unecm.js"></script>
    <script src="qndyd.js"></script>
    <script></script>
`)